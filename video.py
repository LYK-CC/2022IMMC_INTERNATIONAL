import cv2

def visual():    
    data_path = "random_3/"
    fps = 30          # 视频帧率
    size = (620, 480) # 需要转为视频的图片的尺寸
    video = cv2.VideoWriter("output.mp4", cv2.VideoWriter_fourcc('F', 'M', 'P', '4'), fps, size)
    
    for i in range(1,618):      
        image_path = data_path + str(i)+".jpg"
        #print(image_path)
        img = cv2.imread(image_path)
        video.write(img)
    
    video.release()
    cv2.destroyAllWindows()
    
if __name__ == "__main__":
    main()