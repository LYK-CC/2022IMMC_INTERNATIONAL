
from cProfile import label
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator




def draw(seat,indexx,item,filepath):
    #plt.rcParams['savefig.dpi'] = 300 #图片像素
    #plt.rcParams['figure.dpi'] = 300
    #plt.figure(figsize=(10,1))
    ax=plt.subplot(111) #注意:一般都在ax中设置,不再plot中设置
    #plt.grid()
    #x1= np.linspace(0,30, 10)
    #x2=np.linspace(60,90, 10)

    #

    #
    plt.xlim(0, 34)
    plt.ylim(0, 7)



    x = range(20)#TODO
    N =len(x)
    plt.gca().margins(x=0)
    plt.gcf().canvas.draw()
    tl = plt.gca().get_xticklabels()
    # maxsize = max([t.get_window_extent().width for t in tl])
    maxsize = 100
    m = 0.2  # inch margin

    s = maxsize / plt.gcf().dpi * N + 2 * m
    margin = m / plt.gcf().get_size_inches()[0]

    plt.gcf().subplots_adjust(left=margin, right=1. - margin)
    plt.gcf().set_size_inches(s, plt.gcf().get_size_inches()[1])


    plt.gca().set_aspect(1)


    ax.xaxis.set_major_locator(MultipleLocator(1))#设置y主坐标间隔 1
    ax.yaxis.set_major_locator(MultipleLocator(1))#设置y主坐标间隔 1
    ax.xaxis.grid(True,which='major')#major,color='black'
    ax.yaxis.grid(True,which='major')#major,color='black'










    y1=0
    y2=3
    #读取数据
    #pemsdata1=
    #线圈检测器
    for i in range(1):
        x1= np.linspace(i,i+34)
        ax.fill_between(x1,y1,y2,facecolor='red')


    y1=4
    y2=7
    #读取数据
    #pemsdata1=
    #线圈检测器
    for i in range(1):
        x1= np.linspace(i,i+34)
        ax.fill_between(x1,y1,y2,facecolor='red')

    y1=3
    y2=4
    #读取数据
    #pemsdata1=
    #线圈检测器
    for i in range(1):
        x1= np.linspace(i,i+34)
        ax.fill_between(x1,y1,y2,facecolor='yellow')



    i = 0
    for ii in seat:
        for j in range(1,int((len(ii)-1)/2)+1):
            if ii[j]:
                ax.fill_between(np.linspace(i,i+1),j-1,j,facecolor='green')
                plt.text(i,j-1,'%d'%ii[j], ha = 'left',va = 'bottom',fontsize=10,color='white')
        i+=1
    i = 0
    for ii in seat:
        for j in range(int((len(ii)-1)/2)+1,int(len(ii))):
            if ii[j]:
                ax.fill_between(np.linspace(i,i+1),j,j+1,facecolor='green')
                plt.text(i,j,'%d'%ii[j], ha = 'left',va = 'bottom',fontsize=10,color='white')
        i+=1
    
    iterate = 1
    while iterate < len(item) and item[iterate][3]:
        plt.text(item[iterate][3],3,'%d'%item[iterate][6], ha = 'left',va = 'bottom',fontsize=10)
        iterate+=1
    
    plt.savefig(filepath+str(indexx)+".jpg",dpi=300)
    #plt.show() 
    #print(indexx)
    plt.close()
item=np.loadtxt("item.txt")

filepath=''#TODO
seat = [[0, 0, 0, 0, 0, 0, 0], [0, 152, 173, 194, 0, 0, 0], [0, 151, 172, 193, 183, 162, 141], [0, 150, 171, 192, 182, 161, 140], [0, 149, 170, 191, 181, 160, 139], [0, 148, 169, 190, 180, 159, 138], [0, 147, 168, 189, 179, 158, 137], [0, 146, 167, 188, 178, 157, 136], [0, 145, 166, 187, 177, 156, 135], [0, 144, 165, 186, 176, 155, 134], [0, 143, 164, 185, 175, 154, 133], [0, 142, 163, 184, 174, 153, 132], [0, 87, 109, 131, 120, 98, 76], [0, 86, 108, 130, 119, 97, 75], [0, 85, 107, 129, 118, 96, 74], [0, 84, 106, 128, 117, 95, 73], [0, 83, 105, 127, 116, 94, 72], [0, 82, 104, 126, 115, 93, 71], [0, 81, 103, 125, 114, 92, 70], [0, 80, 102, 124, 113, 91, 69], [0, 79, 101, 123, 112, 90, 68], [0, 78, 100, 122, 111, 89, 67], [0, 77, 99, 121, 110, 88, 66], [0, 21, 43, 65, 54, 32, 10], [0, 20, 42, 64, 53, 31, 9], [0, 19, 41, 63, 52, 30, 8], [0, 18, 40, 62, 51, 29, 7], [0, 17, 39, 61, 50, 28, 6], [0, 16, 38, 60, 49, 27, 5], [0, 15, 37, 59, 48, 26, 4], [0, 14, 36, 58, 47, 25, 3], [0, 13, 35, 57, 46, 24, 2], [0, 12, 34, 56, 45, 23, 1], [0, 11, 33, 55, 44, 22, 0]]
draw(seat,1,item,filepath)

    
    
    
    