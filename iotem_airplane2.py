import numpy as np
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import random
from visu import draw

people = 318
printpicture=True
#0 action
#1 row
#2 column
#3 row_now
#4 lug

def checkseat(row,column):
    cnt = 0
    if column < 3:
        for iii in range(column+1,3+1):
            cnt += seatd[row][iii]
    if column > 3+1:
        for iii in range(4,column):
            cnt += seatd[row][iii]  
            #print(iii,row)
    if cnt == 0:
        return 0;
    else:
        return cnt;
    


passenger_row1 = np.loadtxt("airplane2/column1.txt")
passengers_column1 = np.loadtxt("airplane2/row1.txt")
passenger_row2 = np.loadtxt("airplane2/column2.txt")
passengers_column2 = np.loadtxt("airplane2/row2.txt")
passenger_row3 = np.loadtxt("airplane2/column3.txt")
passengers_column3 = np.loadtxt("airplane2/row3.txt")
passenger_row4 = np.loadtxt("airplane2/column4.txt")
passengers_column4 = np.loadtxt("airplane2/row4.txt")
passengers_lug = np.loadtxt("luggage.txt")

item1 = []
item2 = []
item3 = []
item4 = []
seatd = [[0 for j in range(0,25)] for i in range(0,15)]

for i in range(0,people):
    temp = [0,passenger_row1[i],passengers_column1[i],-i,int((passengers_lug[i]+2)/0.6252307692),0,0]
    item1.append(temp)
for i in range(0,people):
    temp = [0,passenger_row2[i],passengers_column2[i],-i,int((passengers_lug[i]+2)/0.6252307692),0,0]
    item2.append(temp)
for i in range(0,people):
    temp = [0,passenger_row3[i],passengers_column3[i],-i,int((passengers_lug[i]+2)/0.6252307692),0,0]
    item3.append(temp)
for i in range(0,people):
    temp = [0,passenger_row4[i],passengers_column4[i],-i,int((passengers_lug[i]+2)/0.6252307692),0,0]
    item4.append(temp)
counttt=0

#random.shuffle(item)
#for i in range(0,people):
#    item[i][3]=-i

item1.insert(0,([0,0,0,1000000,0,0,0]))#stackoverflow
item2.insert(0,([0,0,0,1000000,0,0,0]))#stackoverflow
item3.insert(0,([0,0,0,1000000,0,0,0]))#stackoverflow
item4.insert(0,([0,0,0,1000000,0,0,0]))#stackoverflow
#print(item)

time=0
index=0

#init graph-----------------------------------------------------
if printpicture:
    ax=plt.subplot(111) #注意:一般都在ax中设置,不再plot中设置
    #plt.grid()
    #x1= np.linspace(0,30, 10)
    #x2=np.linspace(60,90, 10)

    #

    #
    plt.xlim(0, 14)
    plt.ylim(0, 24)



    x = range(20)#TODO
    N =len(x)
    plt.gca().margins(x=0)
    plt.gcf().canvas.draw()
    tl = plt.gca().get_xticklabels()
    # maxsize = max([t.get_window_extent().width for t in tl])
    maxsize = 30
    m = 0.1  # inch margin

    s = maxsize / plt.gcf().dpi * N + 2 * m
    margin = m / plt.gcf().get_size_inches()[0]

    plt.gcf().subplots_adjust(left=margin, right=1. - margin)
    plt.gcf().set_size_inches(s, plt.gcf().get_size_inches()[1])


    plt.gca().set_aspect(1)


    ax.xaxis.set_major_locator(MultipleLocator(1))#设置y主坐标间隔 1
    ax.yaxis.set_major_locator(MultipleLocator(1))#设置y主坐标间隔 1
    ax.xaxis.grid(True,which='major')#major,color='black'
    ax.yaxis.grid(True,which='major')#major,color='black'


    y1=0
    y2=24
    #读取数据
    #pemsdata1=
    #线圈检测器
    for i in range(1):
        x1= np.linspace(i,i+14)
        ax.fill_between(x1,y1,y2,facecolor='red')


    #读取数据
    #pemsdata1=
    #线圈检测器
    #读取数据
    #pemsdata1=
    #线圈检测器
    for i in range(1):
        x1= np.linspace(i,i+14)
        ax.fill_between(x1,3,4,facecolor='yellow')
    for i in range(1):
        x1= np.linspace(i,i+14)
        ax.fill_between(x1,7,8,facecolor='yellow')
    for i in range(1):
        x1= np.linspace(i,i+14)
        ax.fill_between(x1,15,16,facecolor='yellow')
    for i in range(1):
        x1= np.linspace(i,i+14)
        ax.fill_between(x1,19,20,facecolor='yellow')
    

        
#end init--------------------------------------------



while people:
    i=-1
    while i<people: 
        i+=1
        if item[i][0] and item[i][4] >= 1: #putting action
            item[i][4]-= 1
            #print(i)
            continue;
        if item[i][4] < 1 and item[i][0]:#seated
            cnt = checkseat(int(item[i][1]),int(item[i][2]))
            #print(cnt)
            #print(seatd)
            #print(int(item[i][1]),int(item[i][2]))
            if cnt == 0:
                
                seatd[int(item[i][1])][int(item[i][2])]+=1
                #draw-----------------

                index+=1
                if printpicture:
                    print(index)
                    if item[i][2]<=3:
                        
                        ax.fill_between(np.linspace(item[i][1],item[i][1]+1),item[i][2]-1,item[i][2],facecolor='green')
                    else:  
                        ax.fill_between(np.linspace(item[i][1],item[i][1]+1),item[i][2],item[i][2]+1,facecolor='green')
                    
                    plt.savefig("bysection_visu/fig"+str(index)+".jpg")
                    #plt.show()
                #print(index)
                #print(seatd)
                #print()
                #print(item)
                #print(item[i][6])
                del item[i]
                i-=1
                people -= 1
                continue;
            else:
                #if item[i][3]+2<item[i-1][3]:
                    #print("1111")
                    #print(item[i][1],item[i][2])
                    #print("\n\n\n\n\n\n\n\n\n\n\n\n")
                item[i][0]=0
                item[i][4] = int((3/0.6252307692)*cnt) #TODO rang zuo time
                item[i][5] = 1 
                continue;
        if item[i][5] and item[i][4] >= 1:
            #item[i][6]+=1
            
            item[i][4]-= 1
            continue;
        if item[i][4] < 1 and item[i][5]:
            seatd[int(item[i][1])][int(item[i][2])]+=1
            #draw-----------------
            
            index+=1
            if printpicture:
                print(index)
                
                if item[i][2] <= 3:
                    ax.fill_between(np.linspace(item[i][1],item[i][1]+1),item[i][2]-1,item[i][2],facecolor='green')
                else:
                    ax.fill_between(np.linspace(item[i][1],item[i][1]+1),item[i][2],item[i][2]+1,facecolor='green')
                plt.savefig("bysection_visu/fig"+str(index)+".jpg")
            #plt.show()
            #print(index)
            #print(seatd)
            #print()
            del item[i]
            i-=1
            people -= 1
            #for ii in seatd:
                #print(ii)
            continue;
        if item[i][3] < item[i][1]:
            if item[i][3] < item[i-1][3]-1:
                item[i][3] += 1
                continue;
        else:
            if item[i][3]==item[i][1] and not item[i][0] and not item[i][5]:
                item[i][0] = 1
                item[i][4] -= 1
        
    time+=1
print(seatd)
    
        
        
    

 








